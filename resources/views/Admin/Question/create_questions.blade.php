@extends('Admin.layouts.master')
@section('add_qns', 'active')
@section('content')

<div class="content">

    <div class="panel panel-flat">

        <div class="panel-heading">
            <h2 class="panel-title text-center" style="border-bottom: 1px solid #DDDDDD; margin-bottom: 10px; margin-right: 10px">Add Questions<span class="label label-success position-right" style="font-size: 14px">
                    @if(Session::has('message'))
                        {{Session::get('message')}}
                    @endif
                </span>
            </h2>
            <div class="heading-elements">
                <span class="label label-primary heading-text"><a href="{{url('/admin/questions/questionlist')}}" style="color: white;font-size: 14px" >My Questions</a></span>
            </div>
        </div>

        <div class="panel-body">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-md-offset-3 col-md-6">

                {!! Form::open(['url' => '/admin/question/store', 'class' =>'form-horizontal'   ]) !!}

                <div class="form-group">
                    {!! Form::label('title','Title') !!}
                    {!! Form::text('title', null,
                        array('class'=>'form-control',
                              'placeholder'=>'Your Title')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('date','Date') !!}
                    {!! Form::date('date', null,
                        array('class'=>'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description','Description') !!}
                    {!! Form::textarea('description', null,
                        array('class'=>'form-control',
                              'placeholder'=>'Your Description')) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Add',
                      array('class'=>'btn btn-primary')) !!}
                </div>
                {!! Form::close() !!}

            </div>

        </div>
    </div>
</div>

@endsection