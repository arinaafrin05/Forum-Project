
<div class="sidebar-user">
    <div class="category-content">
        <div class="media">
            <a href="#" class="media-left"><img src="{{asset('assets/images/placeholder.jpg')}}" class="img-circle img-sm" alt=""></a>
            <div class="media-body">
                <span class="media-heading text-semibold">{{ isset(Auth::user()->name) ? Auth::user()->name : 'NULL' }}</span>
                <div class="text-size-mini text-muted">
                    <i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
                </div>
            </div>

            <div class="media-right media-middle">
                <ul class="icons-list">
                    <li>
                        <a href="#"><i class="icon-cog3"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /User menu -->


<!-- Main navigation -->

<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <li class="active">
                <a href="{{'/admin'}}"><i class="icon-home4"></i> <span>Dashboard</span></a>
            </li>
            <li>
                <a href="#"><i class="icon-stack2"></i> <span>Profile</span></a>
                <ul>
                    <li class="@yield('edit_about')"><a href="{{url('/admin/about/'.Auth::user()->id.'/profile')}}">My Profile</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-stack"></i> <span>Questions</span></a>
                <ul>
                    <li class="@yield('qns_list')"><a href="{{url('/admin/questions/questionlist')}}">My Questions</a></li>
                    <li class="@yield('add_qns')"><a href="{{url('/admin/questions/create')}}">Add New Question</a></li>
                    <li class="@yield('edit_qns')"><a href="{{url('/admin/questions/'.Auth::user()->id.'/edit')}}">Update Question</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-stack"></i> <span>Answers</span></a>
                <ul>
                    <li class="@yield('ans_list')"><a href="{{url('/admin/answers/answerslist')}}">My Answers</a></li>
                    <li class="@yield('add_ans')"><a href="{{url('/admin/answers/create')}}">Add New Answer</a></li>
                    <li class="@yield('edit_ans')"><a href="{{url('/admin/answers/'.Auth::user()->id.'/edit')}}">Update Answer</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>