@include('layouts.includes.head')

<!-- =-=-=-=-=-=-= HOME =-=-=-=-=-=-= -->
<div class="full-section search-section">
    <div class="search-area container">
        <h3 class="search-title">Have a Question?</h3>
        <p class="search-tag-line">If you have any question you can ask below or enter what you are looking for!</p>
        <form autocomplete="off" method="get" action="{{url('/admin/questions/questionlist')}}" class="search-form clearfix" id="search-form">
            <input type="text" name="title" title="* Please enter a search term!" placeholder="Type your search terms here" class="search-term " autocomplete="off">
            <input type="submit" value="Search" class="search-btn">
        </form>
    </div>
</div>
<!-- =-=-=-=-=-=-= HOME END =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Area =-=-=-=-=-=-= -->
<div class="main-content-area">
    <!-- =-=-=-=-=-=-= Latest Questions  =-=-=-=-=-=-= -->
    <section class="white question-tabs padding-bottom-80" id="latest-post">
        <div class="container">
            <div class="row">
                <!-- Content Area Bar -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <!-- Tabs -->
                            <ul class="nav panel-tabs">
                                <li class="active"> <a data-toggle="tab" href="#tab2"><i class="icofont icon-layers"></i><span class="hidden-xs">Questions</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                            <div id="tab2">
                                <!-- Question Listing -->
                                @foreach($allInformation as $question)
                                    <div class="listing-grid">

                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 col-xs-12 hidden-xs">
                                                <a data-toggle="tooltip" data-placement="bottom" data-original-title="Martina Jaz" href="#">
                                                    @if(isset($u_data))
                                                    <img alt="" class="img-responsive center-block" src="{{asset('assets/upload_image/'.$u_data->img)}}">
                                                    @endif
                                                </a>
                                            </div>
                                            <div class="col-md-7 col-sm-8  col-xs-12">
                                                <h3><a  href="#">{{$question->title}}</a></h3>
                                                <div class="listing-meta"> <span><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $question->created_at}}</span>  <span><i class="fa fa fa-eye" aria-hidden="true"></i> {{$question->view}} Views</span>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-2 col-xs-12">
                                                <ul class="question-statistic">
                                                    <li> <a href="{{url('/post-questions/'.$question->id.'/edit')}}" data-toggle="tooltip" data-placement="bottom" data-original-title="Answers"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a>
                                                    </li>
                                                    <li> <a  href="{{url('/post-questions/'.$question->id.'/destroy')}}" onclick="return confirm('Do You Want To Delete?')" data-toggle="tooltip" data-placement="bottom" data-original-title="Votes"><span><i class="fa fa-trash-o" aria-hidden="true"></i></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-10 col-sm-10  col-xs-12">
                                                <p>{{ str_limit($question->description,$limit = 200, $end = '...') }}</p>
                                                <div class="pull-right tagcloud"> <a href="{{url('/post-questions/'.$question->id.'/single-view')}}">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    {{ $allInformation->links() }}
                </div>
            </div>
        </div>
        <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= Latest Questions  End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= SEPARATOR Fun Facts =-=-=-=-=-=-= -->
    <div class="parallex section-padding fun-facts-bg text-center" data-stellar-background-ratio="0.1">
        <div class="container">
            <div class="row">
                <!-- countTo -->
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="statistic-percent" data-perc="356">
                        <div class="facts-icons"> <span class="icon-trophy"></span>
                        </div>
                        <div class="fact"> <span class="percentfactor">356</span>
                            <p>Happy Clients</p>
                        </div>
                        <!-- end fact -->
                    </div>
                    <!-- end statistic-percent -->
                </div>
                <!-- end col-xs-6 col-sm-3 col-md-3 -->
                <!-- countTo -->
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="statistic-percent" data-perc="126">
                        <div class="facts-icons"> <span class="icon-trophy"></span>
                        </div>
                        <div class="fact"> <span class="percentfactor">126</span>
                            <p>Order Received</p>
                        </div>
                        <!-- end fact -->
                    </div>
                    <!-- end statistic-percent -->
                </div>
                <!-- end col-xs-6 col-sm-3 col-md-3 -->
                <!-- countTo -->
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="statistic-percent" data-perc="274">
                        <div class="facts-icons"> <span class="icon-chat"></span>
                        </div>
                        <div class="fact"> <span class="percentfactor">274</span>
                            <p>Free Delivery</p>
                        </div>
                        <!-- end fact -->
                    </div>
                    <!-- end statistic-percent -->
                </div>
                <!-- end col-xs-6 col-sm-3 col-md-3 -->
                <!-- countTo -->
                <div class="col-xs-6 col-sm-3 col-md-3">
                    <div class="statistic-percent" data-perc="434">
                        <div class="facts-icons"> <span class="icon-megaphone"></span>
                        </div>
                        <div class="fact"> <span class="percentfactor">434</span>
                            <p>Completed Projects</p>
                        </div>
                        <!-- end fact -->
                    </div>
                    <!-- end statistic-percent -->
                </div>
                <!-- end col-xs-6 col-sm-3 col-md-3 -->
            </div>
            <!-- End row -->
        </div>
        <!-- end container -->
    </div>
    <!-- =-=-=-=-=-=-= SEPARATOR END =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Testimonials =-=-=-=-=-=-= -->
    <section data-stellar-background-ratio="0.1" class="testimonial-bg parallex">
        <div class="container">
            <!-- Row -->
            <div class="row">
                <!-- Blog Grid -->
                <div class="col-md-8 ">
                    <div id="owl-slider" class="happy-client">
                        <div class="item"> <i class="fa fa-quote-left"> </i>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.</p>
                            <div class="client-info-wrap clearfix">
                                <div class="client-img">
                                    <img class="img-circle" src="../../../theemon.com/d/designagency/LivePreview/assets/images/client-img-two.jpg" alt="" />
                                </div>
                                <div class="client-info"> <strong> Muhammad Umair </strong>  <i class="fa fa-star grey-star"> </i>  <i class="fa fa-star"> </i>  <i class="fa fa-star"> </i>  <i class="fa fa-star"> </i>  <i class="fa fa-star"> </i>
                                </div>
                            </div>
                        </div>
                        <div class="item"> <i class="fa fa-quote-left"> </i>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.</p>
                            <div class="client-info-wrap clearfix">
                                <div class="client-img">
                                    <img class="img-circle" src="../../../theemon.com/d/designagency/LivePreview/assets/images/client-img-two.jpg" alt="" />
                                </div>
                                <div class="client-info"> <strong> Muhammad Umair </strong>  <i class="fa fa-star grey-star"> </i>  <i class="fa fa-star"> </i>  <i class="fa fa-star"> </i>  <i class="fa fa-star"> </i>  <i class="fa fa-star"> </i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Blog Grid -->
                <!-- Blog Grid -->
                <div class="col-md-4 no-padding">
                    <div class="success-stories">
                        <div class="main-heading text-center">
                            <h2>Success Stories</h2>
                            <hr class="main">
                            <p>Cras varius purus in tempus porttitor ut dapibus efficitur sagittis cras vitae lacus metus nunc vulputate facilisis nisi
                                <br>eu lobortis erat consequat ut. Aliquam et justo ante. Nam a cursus velit</p>
                        </div>
                    </div>
                </div>
                <!-- Blog Grid -->
                <div class="clearfix"></div>
            </div>
            <!-- Row End -->
        </div>
        <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= Testimonials  =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Download Apps =-=-=-=-=-=-= -->
    <div class="parallex section-padding  our-apps text-center">
        <div class="container">
            <!-- title-section -->
            <div class="main-heading text-center">
                <h2>Download Our Apps</h2>
                <hr class="main">
            </div>
            <!-- End title-section -->
            <div class="row">
                <div class="app-content">
                    <!-- Single download start \-->
                    <div class="col-md-4 col-sm-4">
                        <a href="#" class="app-grid"> <span class="app-icon"> <img alt="#" src="images/mobile.png"> </span>
                            <div class="app-title">
                                <h5>Available on the</h5>
                                <h3>iOS App Store</h3>
                            </div>
                        </a>
                    </div>
                    <!--/ Single download end-->
                    <!-- Single download start \-->
                    <div class="col-md-4 col-sm-4">
                        <a href="#" class="app-grid"> <span class="app-icon"> <img alt="#" src="images/play-store.png"> </span>
                            <div class="app-title">
                                <h5>Available on the</h5>
                                <h3>Google Store</h3>
                            </div>
                        </a>
                    </div>
                    <!--/ Single download end-->
                    <!-- Single download start \-->
                    <div class="col-md-4  col-sm-4">
                        <a href="#" class="app-grid"> <span class="app-icon"> <img alt="#" src="images/windows.png"> </span>
                            <div class="app-title">
                                <h5>Available on the</h5>
                                <h3>Windows Store</h3>
                            </div>
                        </a>
                    </div>
                    <!--/ Single download end-->
                </div>
            </div>
            <!-- End row -->
        </div>
        <!-- end container -->
    </div>
    <!-- =-=-=-=-=-=-= Download Apps END =-=-=-=-=-=-= -->

</div>
<!-- =-=-=-=-=-=-= Main Area End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
@include('layouts.includes.footer')