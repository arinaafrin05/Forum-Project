<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class settingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($id)
    {
        $data=Setting::find($id);
        return view('layouts.Users.settings',compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->except('img');
        $mydata= Setting::find($id);

        if ($request->hasFile('img')){
            $image = $request->file('img');
            $filename = time().'.'. $image->getClientOriginalExtension();
            $location = public_path('assets/upload_image/'.$filename);
            Image::make($image)->resize(600,420)->save($location);

            $data['img'] = $filename;
            $oldfilename = $mydata->img;
            Storage::delete($oldfilename);
        }
//dd($data);
        $mydata->update($data);
        Session::flash('message','Profile Update Successfully');
        return redirect()->back();
    }
}
