<?php

namespace App\Http\Controllers;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $text= "Welcome to my application";
        return view('Admin.dashboard',['key'=>$text]);
    }
    public function questionlogin()
    {
        $users_id = Auth::user()->id;
        $users=DB::table('questions')
            ->join('settings', 'questions.user_id','=','settings.user_id')
            ->join('users','settings.user_id','=','users.id')
            ->where('users.id','=',$users_id)
            ->get();
//        dd($users);
        foreach ($users as $u_data){
        }

        $allInformation = Question::orderBy('id', 'DESC')->paginate(3);
//        dd($allInformation);
        $data =[
            'allInformation'=>$allInformation,
            'u_data'=>$u_data,
        ];
        return view('welcome',$data);
    }

}
