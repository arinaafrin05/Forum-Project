<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password','img'
    ];
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
    public function settings()
    {
        return $this->hasMany(Setting::class);
    }
    public function questions()
    {
        return $this->hasMany(Question::class);
    }
    protected $hidden = [
        'password', 'remember_token',
    ];

}
