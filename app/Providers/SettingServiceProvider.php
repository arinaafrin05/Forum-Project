<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class SettingServiceProvider extends ServiceProvider
{

    public function boot()
    {
        View::composer('profile','App\Http\ViewSetting\ProfileComposer');
    }


    public function register()
    {

    }
}
