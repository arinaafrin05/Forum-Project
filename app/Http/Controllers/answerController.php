<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class answerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['user_id']=Auth::user()->id;
        $this->validate($request, [
            'title'=>'required',
            'date' => 'required',
            'description' => 'required',
        ]);
        Answer::create($data);
        Session::flash('message','Answer Submitted successfully');
        return redirect()->back();

    }
    public function edit($id)
    {
        $data = Answer::find($id);
        return view('layouts.Questions.ansedit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $mydata= Answer::find($id);
        $mydata->update($data);
        Session::flash('message','Update Successfully');
        return redirect()->back();
    }
    public function destroy($id)
    {
        $data = Answer::find($id);
        $data->delete();
        Session::flash('message','Deleted Successfully');
        return redirect()->back();
    }

}
