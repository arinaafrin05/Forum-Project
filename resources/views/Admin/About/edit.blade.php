@extends('Admin.layouts.master')
@section('edit_about', 'active')
@section('content')

    <div class="content">

        <div class="panel panel-flat">

            <div class="panel-heading">
                <h2 class="panel-title text-center" style="border-bottom: 1px solid #DDDDDD; margin-bottom: 10px; margin-right: 10px">My Profile<span class="label label-success position-right" style="font-size: 14px">
                    @if(Session::has('message'))
                            {{Session::get('message')}}
                        @endif
                </span>
                </h2>
            </div>

            <div class="panel-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-md-offset-3 col-md-6">

                    {!! Form::model($data,['url' => '/admin/about/'.$data['id'].'/update', 'class' =>'form-horizontal','files' => true   ]) !!}


                    <div class="form-group">
                        {!! Form::label('name','Full Name') !!}
                        {!! Form::text('name', null,
                            array('class'=>'form-control',
                                  'placeholder'=>'Your Full name')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('date','Date of Birth') !!}
                        {!! Form::date('date', null,
                            array('class'=>'form-control',
                                  'placeholder'=>'Your Date of birth')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('description','Description') !!}
                        {!! Form::textarea('description', null,
                            array('class'=>'form-control',
                                  'placeholder'=>'Your Description')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('contact','Contact') !!}
                        {!! Form::text('contact', null,
                            array('class'=>'form-control',
                                  'placeholder'=>'Your Contact Number')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('hobby','Your Hobby') !!}
                        {!! Form::text('hobby', null,
                            array('class'=>'form-control',
                                  'placeholder'=>'Your Hobby')) !!}
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Current Image : </label>
                        <div class="col-md-9">
                            <img src="{{asset('assets/upload_image/'.$data->img)}}" alt="Current image" width="100" height="100" />
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('Image Upload') !!}
                        {!! Form::file('img', null,
                            array('class'=>'form-control',
                                  'placeholder'=>'Your Image')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Update',
                          array('class'=>'btn btn-primary')) !!}
                    </div>
                    {!! Form::close() !!}

                </div>

            </div>
        </div>
    </div>

@endsection