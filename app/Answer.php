<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable=['id','qns_id','title','date','description','user_id'];
    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
