@include('layouts.includes.head')

    <!-- =-=-=-=-=-=-= Register Form =-=-=-=-=-=-= -->
    <section class="section-padding-80 white" id="register">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">

                    <div class="box-panel">

                        <!-- buttons top -->
                        <a href="#" class="btn btn-default facebook"><i class="fa fa-facebook icons"></i> Sign Up with Facebook</a>
                        <a href="#" class="btn btn-default google"><i class="fa fa-google-plus icons"></i> Sign Up with Google</a>
                        <!-- end buttons top -->

                        <p class="text-center margin-top-10"><span class="span-line">OR</span>
                        </p>

                        <!-- form login -->
                        <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name">Name</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Your Full Name"/>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">E-Mail Address</label>

                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Your Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password">Password</label>

                                <input id="password" type="password" class="form-control" name="password" required placeholder="Your Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password-confirm">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Verify Your Password">

                            </div>

                            {{--<div class="form-group{{ $errors->has('img') ? ' has-error' : '' }}">--}}
                                {{--<label for="password">Upload Image</label>--}}

                                {{--<input id="password" type="file" class="form-control" name="img" required>--}}

                                {{--@if ($errors->has('img'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('img') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!-- form login -->
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <!-- end container -->
    <!-- =-=-=-=-=-=-= Register Form End =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Our Clients =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Our Clients -end =-=-=-=-=-=-= -->

</div>
    </section>
<!-- =-=-=-=-=-=-= Main Area End =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
@include('layouts.includes.footer')