@include('layouts.includes.dashboardhead')

    <!-- =-=-=-=-=-=-= Post Question  =-=-=-=-=-=-= -->
    <section class="section-padding-80 white" id="post-question">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 ">

                    <div class="box-panel">

                        <h2>Post Your Question
                            <button class="btn btn-success">
                                @if(Session::has('message'))
                                    {{Session::get('message')}}
                                @endif
                            </button>
                        </h2>
                        <p>Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. </p>
                        <hr>
                        <!-- form login -->
                        <form class="margin-top-40" role="form" method="POST" action="{{ url('/post-questions/store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title">Question Title</label>
                                <input type="text" class="form-control" name="title" required autofocus placeholder="Ex: Bootstrap not working"/>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                <label for="date">Date</label>

                                <input type="date" class="form-control" name="date" required>

                                @if ($errors->has('date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description">Question Details</label>

                                <textarea cols="12" rows="12" placeholder="Post Your Question Details Here....." name="description" class="form-control"></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                    <button type="submit" class="btn btn-primary pull-right">
                                        Publish Your Question
                                    </button>
                            </div>
                        </form>
                        <!-- form login -->

                    </div>
                </div>

                <!-- Blog Right Sidebar -->
                <div class="col-sm-12 col-xs-12 col-md-4">

                    <!-- sidebar -->
                    <div class="side-bar">

                        <!-- widget -->
                        <div class="widget">
                            <div class="recent-comments">
                                <h2>Hot Questions</h2>
                                <hr class="widget-separator no-margin">
                                <ul>
                                    <li><a href="#">Twitter Bootsrap 3.0 - tabs-left not working anymore?</a>
                                    </li>
                                    <li><a href="#">Changing the color on my tabbed left bootstrap3 tabs</a>
                                    </li>
                                    <li><a href="#">How to create tabs on the left in bootstrap.js v3.0.0? </a>
                                    </li>
                                    <li><a href="#">A Repository class - would you use it to handle multi records? </a>
                                    </li>

                                    <li><a href="#">post data from html form to php script and return result to ajax/js/jquery </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- widget -->

                        <!-- widget -->
                        <div class="widget">
                            <div class="latest-news">
                                <h2>Latest News</h2>
                                <hr class="widget-separator no-margin">

                                <div class="news-post">
                                    <div class="post">
                                        <figure class="post-thumb"><img alt="" src="images/blog/small-1.png">
                                        </figure>
                                        <h4><a href="#">Differentiate Yourself And Attract More Attention </a></h4>
                                        <div class="post-info">1 hour ago</div>
                                    </div>

                                    <div class="post">
                                        <figure class="post-thumb"><img alt="" src="images/blog/small-2.png">
                                        </figure>
                                        <h4><a href="#">Differentiate Yourself And Attract More Attention </a></h4>
                                        <div class="post-info">1 hour ago</div>
                                    </div>

                                    <div class="post">
                                        <figure class="post-thumb"><img alt="" src="images/blog/small-3.png">
                                        </figure>
                                        <h4><a href="#">Differentiate Yourself And Attract More Attention </a></h4>
                                        <div class="post-info">1 hour ago</div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- widget end -->

                    </div>
                    <!-- sidebar end -->

                </div>
                <!-- Blog Right Sidebar End -->
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= Post QuestionEnd =-=-=-=-=-=-= -->

</div>
<!-- =-=-=-=-=-=-= Main Area End =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
@include('layouts.includes.footer')